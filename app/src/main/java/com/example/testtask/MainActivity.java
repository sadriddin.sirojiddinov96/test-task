package com.example.testtask;

import androidx.annotation.RequiresApi;
import androidx.room.Room;

import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.example.testtask.base.BaseActivity;
import com.example.testtask.fragment.UserDetailsFragment;
import com.example.testtask.fragment.UserFragment;
import com.example.testtask.model.UserDto;
import com.example.testtask.presenter.UserPresenter;
import com.example.testtask.presenter.UserPresenterImpl;
import com.example.testtask.repo.UserRepoImpl;
import com.example.testtask.repo.UsersRepo;
import com.example.testtask.repo.entity.User;
import com.example.testtask.repo.local.DBConstant;
import com.example.testtask.repo.local.LocalDB;
import com.example.testtask.repo.local.UsersLocalRepo;
import com.example.testtask.repo.local.UsersLocalRepoImpl;
import com.example.testtask.repo.pojo.DataResponse;
import com.example.testtask.repo.pojo.UserDetailResponse;
import com.example.testtask.repo.remote.UsersRemoteRepo;
import com.example.testtask.repo.remote.UsersRemoteRepoImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import io.reactivex.android.schedulers.AndroidSchedulers;

public class MainActivity extends BaseActivity<UserPresenter> implements UsersView, UserFragment.UserFragmentListener {
    private static final String TAG = "Sadriddin";

    List<User> currentUsers;

    @Override
    protected UserPresenter createPresenter() {

        LocalDB localDB = Room.databaseBuilder(getApplicationContext(),LocalDB.class, DBConstant.DB_NAME).build();

        UsersRemoteRepo remoteRepo = new UsersRemoteRepoImpl(this);
        UsersLocalRepo localRepo =new UsersLocalRepoImpl(localDB.userDao());
        UsersRepo usersRepo = new UserRepoImpl(remoteRepo, localRepo);

        return new UserPresenterImpl(usersRepo, AndroidSchedulers.mainThread());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getPresenter().getUsers();
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void showUsers(DataResponse users) {
        List<User> userList = new ArrayList<>(users.getData().stream().map(User::fromUserItem).collect(Collectors.toList()));
        currentUsers = userList;
        Log.d(TAG, "showUsers: Corvo  11" + userList.size());
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, UserFragment.getInstance(userList.stream().map(UserDto::fromUser).collect(Collectors.toList())))
                .commit();
        UserFragment.setListener(this);
    }

    @Override
    public void showUserDetails(UserDetailResponse userDetailResponse) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, UserDetailsFragment.getInstance(userDetailResponse))
                .commit();
    }


    @Override
    public void showLoading() {

        Log.d(TAG, "showLoading() returned: ");

    }

    @Override
    public void hideLoading() {

        Log.d(TAG, "hideLoading() returned: ");

    }

    @Override
    public void showError(String error) {

        Log.d(TAG, "showError() returned: " + error);

    }

    @Override
    public void onItemClickListener(Long userId) {
        Toast.makeText(this, "ID: " + userId, Toast.LENGTH_SHORT).show();
        getPresenter().setUserId(userId);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onBackPressed() {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, UserFragment.getInstance(currentUsers.stream().map(UserDto::fromUser).collect(Collectors.toList())))
                .commit();
        UserFragment.setListener(this);
    }
}
