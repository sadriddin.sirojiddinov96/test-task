package com.example.testtask;

import com.example.testtask.base.MvpView;
import com.example.testtask.repo.pojo.DataResponse;
import com.example.testtask.repo.pojo.UserDetailResponse;

import java.util.List;

public interface UsersView extends MvpView {
    void showUsers(DataResponse userList);
    void showUserDetails(UserDetailResponse userDetailResponse);
    void showLoading();
    void hideLoading();
    void showError(String error);
}
