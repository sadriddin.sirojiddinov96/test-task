package com.example.testtask.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.testtask.R;
import com.example.testtask.model.UserDto;

import java.util.ArrayList;

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.MyViewHolder> {

    private ArrayList<UserDto> userList;
    private Context mContext;


    public UserAdapter(ArrayList<UserDto> userList, Context context) {
        this.userList = userList;
        mContext = context;
    }

    @Override
    public UserAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                       int viewType) {
        View v =  LayoutInflater.from(parent.getContext())
                .inflate(R.layout.user_item, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Glide
                .with(mContext)
                .load(userList.get(position).getImgUrl())
                .centerCrop()
                .placeholder(R.drawable.sample_img)
                .into(holder.userImgView);
        holder.userFullName.setText(userList.get(position).getFullName());

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return userList.size();
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        ImageView userImgView;
        TextView userFullName;
        MyViewHolder(View itemView) {
            super(itemView);
            userImgView = itemView.findViewById(R.id.userImg);
            userFullName = itemView.findViewById(R.id.userFullName);
        }
    }

}