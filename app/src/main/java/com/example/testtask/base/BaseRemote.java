package com.example.testtask.base;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.readystatesoftware.chuck.ChuckInterceptor;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class BaseRemote {


    protected <T> T create(Class<T> clazz, String baseUrl, Context context) {
        T service = retrofit(baseUrl, context).create(clazz);
        return service;
    }

    private Retrofit retrofit(String baseUrl, Context context) {
        OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                .addInterceptor(new ChuckInterceptor(context))
                .build();

        GsonBuilder gsonBuilder = new GsonBuilder();

        Gson customGson = gsonBuilder.create();

        return new Retrofit.Builder()
                .baseUrl(baseUrl).client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(customGson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }

}