package com.example.testtask.fragment;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.testtask.R;
import com.example.testtask.repo.pojo.UserDetailResponse;

/**
 * A simple {@link Fragment} subclass.
 */
public class UserDetailsFragment extends Fragment {

    private static final String USER_KEY = "user key";

    private UserDetailResponse userDetail;

    private TextView nameTV;

    private TextView lastNameTV;

    private TextView genderTV;

    private TextView phoneTV;

    private TextView mailTV;

    private ImageView imgAvatar;


    public UserDetailsFragment() {
        // Required empty public constructor
    }

    public static UserDetailsFragment getInstance(UserDetailResponse userDetail){

        UserDetailsFragment fragment = new UserDetailsFragment();
        Bundle args = new Bundle();
        args.putSerializable(USER_KEY,userDetail);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (getArguments() != null){
            userDetail = (UserDetailResponse) getArguments().getSerializable(USER_KEY);
        }
        return inflater.inflate(R.layout.fragment_user_details, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);
        if (userDetail!=null){
            nameTV.setText(userDetail.getFirstName());
            lastNameTV.setText(userDetail.getLastName());
            genderTV.setText(userDetail.getGender());
            phoneTV.setText(userDetail.getPhone());
            mailTV.setText(userDetail.getEmail());
            Glide
                    .with(view.getContext())
                    .load(userDetail.getImage())
                    .centerCrop()
                    .placeholder(R.drawable.sample_img)
                    .into(imgAvatar);
        }
    }

    private void initViews(View view) {

        nameTV = view.findViewById(R.id.nameTV);
        lastNameTV = view.findViewById(R.id.lastNameTV);
        genderTV = view.findViewById(R.id.genderTV);
        phoneTV = view.findViewById(R.id.phoneTV);
        mailTV = view.findViewById(R.id.birthTV);
        imgAvatar = view.findViewById(R.id.imgAvatar);

    }
}
