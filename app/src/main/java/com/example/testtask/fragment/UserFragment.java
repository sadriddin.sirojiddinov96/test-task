package com.example.testtask.fragment;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.testtask.R;
import com.example.testtask.adapter.UserAdapter;
import com.example.testtask.helper.RecyclerItemClickListener;
import com.example.testtask.model.UserDto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class UserFragment extends Fragment {

    private final static String USERLIST = "key for user list";
    private ArrayList<UserDto> userList;

    private RecyclerView userRecyclerView;

    private static UserFragmentListener mListener;


    public UserFragment() {
        // Required empty public constructor
    }

    public static UserFragment getInstance(List<UserDto> userList){
        UserFragment fragment = new UserFragment();
        Bundle arg = new Bundle();
        arg.putSerializable(USERLIST, (Serializable)userList);
        fragment.setArguments(arg);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (getArguments() != null){
            userList = (ArrayList<UserDto>) getArguments().getSerializable(USERLIST);
        }
        return inflater.inflate(R.layout.fragment_user, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);
        userRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(view.getContext(), userRecyclerView, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                mListener.onItemClickListener(userList.get(position).getId());
            }

            @Override
            public void onLongItemClick(View view, int position) {

            }
        }));
    }

    private void initViews(View view) {
        userRecyclerView = view.findViewById(R.id.userRecycler);
        UserAdapter userAdapter = new UserAdapter(userList,view.getContext());
        userRecyclerView.setAdapter(userAdapter);
    }

    public static void setListener(UserFragmentListener listener){
        mListener = listener;
    }

    public interface UserFragmentListener{
        void onItemClickListener(Long userId);
    }
}
