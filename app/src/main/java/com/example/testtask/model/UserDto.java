package com.example.testtask.model;

import com.example.testtask.repo.entity.User;

import java.io.Serializable;


public class UserDto implements Serializable {

    public UserDto() {
    }

    private Long id;

    private String fullName;

    private String imgUrl;

    public static UserDto fromUser(User user){
        UserDto userDto = new UserDto();
        userDto.setId((long) user.getId());
        userDto.setImgUrl(user.getImage());
        userDto.setFullName(user.getNameTitle() + " " + user.getFirstName() + " " + user.getLastName());
        return userDto;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }
}
