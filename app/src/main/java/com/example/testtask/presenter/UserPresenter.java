package com.example.testtask.presenter;


import com.example.testtask.UsersView;
import com.example.testtask.base.BasePresenter;

public abstract class UserPresenter extends BasePresenter<UsersView> {

    public abstract void getUsers();

    public abstract void getUserDetails();

    public abstract void setUserId(Long userId);

}
