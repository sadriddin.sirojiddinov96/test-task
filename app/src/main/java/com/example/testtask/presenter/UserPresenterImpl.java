package com.example.testtask.presenter;

import com.example.testtask.repo.UsersRepo;
import com.example.testtask.repo.pojo.DataResponse;
import com.example.testtask.repo.pojo.UserDetailResponse;

import java.util.List;

import io.reactivex.Scheduler;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;

public class UserPresenterImpl extends UserPresenter{

    private UsersRepo usersRepo;

    private Scheduler scheduler;

    private Long userId;

    private Disposable disposable;

    public UserPresenterImpl(UsersRepo usersRepo, Scheduler scheduler) {
        this.usersRepo = usersRepo;
        this.scheduler = scheduler;
    }

    @Override
    public void getUsers() {
        if (!isViewAttached())
            return;

        getView().showLoading();

        disposable = usersRepo.getAllUsers().observeOn(scheduler).subscribeWith(new DisposableObserver<DataResponse>() {
            @Override
            public void onNext(DataResponse value) {
                if (!isViewAttached())
                    return;
                getView().showUsers(value);
            }

            @Override
            public void onError(Throwable e) {
                if (!isViewAttached())
                    return;
                getView().showError(e.getLocalizedMessage());
            }

            @Override
            public void onComplete() {

            }
        });
    }

    @Override
    public void getUserDetails() {
        if (!isViewAttached())return;

        getView().showLoading();
        disposable = usersRepo.getUserDetail(userId).observeOn(scheduler).subscribeWith(new DisposableObserver<UserDetailResponse>() {
            @Override
            public void onNext(UserDetailResponse value) {
                if (!isViewAttached())
                    return;
                getView().showUserDetails(value);
            }

            @Override
            public void onError(Throwable e) {
                if (!isViewAttached())
                    return;
                getView().showError(e.getLocalizedMessage());
            }

            @Override
            public void onComplete() {

            }
        });
    }

    @Override
    public void setUserId(Long userId) {
        this.userId = userId;
        getUserDetails();
    }

}
