package com.example.testtask.repo;

import android.nfc.Tag;
import android.os.Build;
import android.util.Log;

import androidx.annotation.RequiresApi;

import com.example.testtask.repo.entity.User;
import com.example.testtask.repo.local.UsersLocalRepo;
import com.example.testtask.repo.pojo.DataResponse;
import com.example.testtask.repo.pojo.UserDetailResponse;
import com.example.testtask.repo.pojo.UserItem;
import com.example.testtask.repo.remote.UsersRemoteRepo;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import io.reactivex.Observable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;


public class UserRepoImpl implements UsersRepo {
    UsersRemoteRepo remoteRepo;
    UsersLocalRepo localRepo;

    public UserRepoImpl(UsersRemoteRepo remoteRepo, UsersLocalRepo localRepo) {
        this.remoteRepo = remoteRepo;
        this.localRepo = localRepo;
    }

    @Override
    public Observable<DataResponse> getAllUsers() {

        return Observable.mergeDelayError(remoteRepo.getAllUsers().doOnNext(
                new Consumer<DataResponse>() {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public void accept(DataResponse users) throws Exception {
                        localRepo.addUsers(users);
                    }
                }).subscribeOn(Schedulers.io()),localRepo.getAllUsers().subscribeOn(Schedulers.io())
        );
    }

    @Override
    public Observable<UserDetailResponse> getUserDetail(Long userId) {
        return Observable.mergeDelayError(remoteRepo.getUserDetail(userId).doOnNext(
                userDetailResponse -> localRepo.getUserDetail(userId)
        ).subscribeOn(Schedulers.io()),localRepo.getUserDetail(userId));
    }
}
