package com.example.testtask.repo;

import com.example.testtask.repo.entity.User;
import com.example.testtask.repo.pojo.DataResponse;
import com.example.testtask.repo.pojo.UserDetailResponse;

import java.util.List;

import io.reactivex.Observable;

public interface UsersRepo {

    Observable<DataResponse> getAllUsers();

    Observable<UserDetailResponse> getUserDetail(Long userId);



}
