package com.example.testtask.repo.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.example.testtask.repo.local.DBConstant;
import com.example.testtask.repo.pojo.UserItem;


@Entity(tableName = DBConstant.USERS_TABLE_NAME)
public class User {

    public static User fromUserItem(UserItem userItem){
        User user = new User();
        user.setId(userItem.getId());
        user.setNameTitle(userItem.getNameTitle());
        user.setFirstName(userItem.getFirstName());
        user.setLastName(userItem.getLastName());
        user.setImage(userItem.getImage());
        return user;
    }

    public User() {
    }

    @PrimaryKey
    @ColumnInfo(name = DBConstant.USER_ID)
    private int id;

    @ColumnInfo(name = DBConstant.NAME_TITLE)
    private String nameTitle;

    @ColumnInfo(name = DBConstant.USER_FIRST_NAME)
    private String firstName;

    @ColumnInfo(name = DBConstant.LAST_NAME)
    private String lastName;

    @ColumnInfo(name = DBConstant.IMG_URL)
    private String image;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNameTitle() {
        return nameTitle;
    }

    public void setNameTitle(String nameTitle) {
        this.nameTitle = nameTitle;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
