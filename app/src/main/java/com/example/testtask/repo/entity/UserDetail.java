package com.example.testtask.repo.entity;

import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.example.testtask.repo.pojo.UserDetailResponse;


@Entity
public class UserDetail {

    public UserDetail() {
    }

    public static UserDetail fromUserDetailResponse(UserDetailResponse userDetailResponse){
        UserDetail userDetail = new UserDetail();
        userDetail.setId( userDetailResponse.getId());
        userDetail.setGender(userDetailResponse.getGender());
        userDetail.setFirstName(userDetailResponse.getFirstName());
        userDetail.setLastName(userDetailResponse.getLastName());
        userDetail.setNameTitle(userDetailResponse.getNameTitle());
        userDetail.setPhone(userDetailResponse.getPhone());
        userDetail.setUsername(userDetailResponse.getUsername());
        userDetail.setEmail(userDetailResponse.getEmail());
        userDetail.setLocation(UserLocation.fromUserLocationResponse(userDetailResponse.getUserLocationResponse()));

        return userDetail;
    }

    @PrimaryKey
    private int id;

    private String gender;

    @Embedded
    private UserLocation location;

    private String email;

    private String dob;

    private String phone;

    private String cell;

    private String image;

    private String firstName;

    private String lastName;

    private String nameTitle;

    private String username;

    private String password;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public UserLocation getLocation() {
        return location;
    }

    public void setLocation(UserLocation location) {
        this.location = location;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCell() {
        return cell;
    }

    public void setCell(String cell) {
        this.cell = cell;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getNameTitle() {
        return nameTitle;
    }

    public void setNameTitle(String nameTitle) {
        this.nameTitle = nameTitle;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
