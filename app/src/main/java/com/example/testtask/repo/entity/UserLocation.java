package com.example.testtask.repo.entity;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.example.testtask.repo.pojo.UserLocationResponse;

@Entity
public class UserLocation {

    public UserLocation() {
    }

    public static UserLocation fromUserLocationResponse(UserLocationResponse userLocationResponse){
        UserLocation userLocation = new UserLocation();
        userLocation.setStreet(userLocationResponse.getStreet());
        userLocation.setCity(userLocationResponse.getCity());
        return userLocation;
    }

    @PrimaryKey
    @NonNull
    private String street;

    private String city;

    private String state;

    private String postcode;

    private String timezone;

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }
}
