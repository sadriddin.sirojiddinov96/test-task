package com.example.testtask.repo.local;

public interface DBConstant {

    String DB_NAME = "database_tet_task";
    String USERS_TABLE_NAME = "Users";

    //User table fields
    String USER_ID = "id";
    String USER_FIRST_NAME = "first_name";
    String LAST_NAME = "last_name";
    String IMG_URL = "image_url";
    String NAME_TITLE = "name_title";

}
