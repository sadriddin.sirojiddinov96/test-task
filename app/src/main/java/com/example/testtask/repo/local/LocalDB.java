package com.example.testtask.repo.local;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.example.testtask.repo.entity.User;
import com.example.testtask.repo.entity.UserDetail;
import com.example.testtask.repo.entity.UserLocation;

@Database(entities = {User.class, UserDetail.class, UserLocation.class}, version = 1, exportSchema = false)
public abstract class LocalDB extends RoomDatabase {

    public abstract UserDao userDao();

    public abstract UserDetailDao userDetailDao();
    //public abstract UserDetailDao userDetailDao();

}
