package com.example.testtask.repo.local;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.example.testtask.repo.entity.User;

import java.util.List;

@Dao
public interface UserDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void addUser(User user);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void addAll(List<User> dataList);

    @Delete
    void removeUser(User user);

    @Update(onConflict =  OnConflictStrategy.REPLACE)
    void uptadeUser(User user);

    @Query("select * from users")
    List<User> loadAllUser();



}