package com.example.testtask.repo.local;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.example.testtask.repo.entity.User;
import com.example.testtask.repo.entity.UserDetail;

import java.util.List;

@Dao
public interface UserDetailDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void addUser(UserDetail userDetail);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void addAll(List<UserDetail> userDetails);

    @Delete
    void removeUser(UserDetail userDetail);

    @Update(onConflict =  OnConflictStrategy.REPLACE)
    void uptadeUserDetail(UserDetail user);

    @Query("select * from userdetail where id=:user_id")
    UserDetail getUserDetail(Long user_id);

    @Query("select * from userdetail")
    List<UserDetail> loadAllUserDetail();



}