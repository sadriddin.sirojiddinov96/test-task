package com.example.testtask.repo.local;

import com.example.testtask.repo.entity.UserDetail;
import com.example.testtask.repo.pojo.DataResponse;
import com.example.testtask.repo.pojo.UserDetailResponse;

import io.reactivex.Observable;

public interface UsersLocalRepo {

    Observable<DataResponse> getAllUsers();
    void addUsers(DataResponse userList);

    Observable<UserDetailResponse> getUserDetail(Long userId);
    void addUserDetails(UserDetailResponse userDetail);

}
