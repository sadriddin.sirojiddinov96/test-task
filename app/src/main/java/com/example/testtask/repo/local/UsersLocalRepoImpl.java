package com.example.testtask.repo.local;

import android.os.Build;

import androidx.annotation.RequiresApi;

import com.example.testtask.repo.entity.User;
import com.example.testtask.repo.entity.UserDetail;
import com.example.testtask.repo.pojo.DataResponse;
import com.example.testtask.repo.pojo.UserDetailResponse;
import com.example.testtask.repo.pojo.UserItem;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.stream.Collectors;

import io.reactivex.Observable;

public class UsersLocalRepoImpl implements UsersLocalRepo {

    private UserDao userDao;
    private UserDetailDao userDetailDao;

    public UsersLocalRepoImpl(UserDao userDao) {
        this.userDao = userDao;
    }

    public UsersLocalRepoImpl(UserDetailDao userDetailDao) {
        this.userDetailDao = userDetailDao;
    }

    @Override
    public Observable<DataResponse> getAllUsers() {
        return Observable.fromCallable(new Callable<DataResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public DataResponse call() throws Exception {
                DataResponse dataResponse = new DataResponse();
                dataResponse.setData(userDao.loadAllUser().stream().map(UserItem::fromUser).collect(Collectors.toList()));
                return dataResponse;
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void addUsers(DataResponse users) {
        List<User> userList = new ArrayList<>();
            userList.addAll(users.getData().stream().map(User::fromUserItem).collect(Collectors.toList()));
        userDao.addAll(userList);
    }


    @Override
    public Observable<UserDetailResponse> getUserDetail(Long userId) {
        return Observable.fromCallable(new Callable<UserDetailResponse>() {
            @Override
            public UserDetailResponse call() throws Exception {
                return UserDetailResponse.fromUserDetail(userDetailDao.getUserDetail(userId));
            }
        });
    }

    @Override
    public void addUserDetails(UserDetailResponse userDetail) {
        userDetailDao.addUser(UserDetail.fromUserDetailResponse(userDetail));
    }

}
