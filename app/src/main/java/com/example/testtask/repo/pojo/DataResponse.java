package com.example.testtask.repo.pojo;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class DataResponse {

	@SerializedName("total")
	private int total;

	@SerializedName("data")
	private List<UserItem> data;

	@SerializedName("limit")
	private int limit;

	@SerializedName("page")
	private int page;

	public void setTotal(int total){
		this.total = total;
	}

	public int getTotal(){
		return total;
	}

	public void setData(List<UserItem> data){
		this.data = data;
	}

	public List<UserItem> getData(){
		return data;
	}

	public void setLimit(int limit){
		this.limit = limit;
	}

	public int getLimit(){
		return limit;
	}

	public void setPage(int page){
		this.page = page;
	}

	public int getPage(){
		return page;
	}

	@Override
 	public String toString(){
		return 
			"DataResponse{" +
			"total = '" + total + '\'' + 
			",data = '" + data + '\'' + 
			",limit = '" + limit + '\'' + 
			",page = '" + page + '\'' + 
			"}";
		}
}