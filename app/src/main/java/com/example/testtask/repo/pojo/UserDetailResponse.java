package com.example.testtask.repo.pojo;

import com.example.testtask.repo.entity.UserDetail;
import com.example.testtask.repo.entity.UserLocation;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class UserDetailResponse implements Serializable {

	public static UserDetailResponse fromUserDetail(UserDetail userDetail){
		UserDetailResponse userDetailResponse = new UserDetailResponse();
		userDetailResponse.setId(userDetail.getId());
		userDetailResponse.setGender(userDetail.getGender());
		userDetailResponse.setFirstName(userDetail.getFirstName());
		userDetailResponse.setLastName(userDetail.getLastName());
		userDetailResponse.setNameTitle(userDetail.getNameTitle());
		userDetailResponse.setPhone(userDetail.getPhone());
		userDetailResponse.setUsername(userDetail.getUsername());
		userDetailResponse.setEmail(userDetail.getEmail());
		userDetailResponse.setUserLocationResponse(UserLocationResponse.fromUserLocation(userDetail.getLocation()));
		return userDetailResponse;
	}


	@SerializedName("image")
	private String image;

	@SerializedName("lastName")
	private String lastName;

	@SerializedName("gender")
	private String gender;

	@SerializedName("nameTitle")
	private String nameTitle;

	@SerializedName("cell")
	private String cell;

	@SerializedName("firstName")
	private String firstName;

	@SerializedName("password")
	private String password;

	@SerializedName("phone")
	private String phone;

	@SerializedName("dob")
	private String dob;

	@SerializedName("userLocationResponse")
	private UserLocationResponse userLocationResponse;

	@SerializedName("id")
	private int id;

	@SerializedName("email")
	private String email;

	@SerializedName("username")
	private String username;

	public void setImage(String image){
		this.image = image;
	}

	public String getImage(){
		return image;
	}

	public void setLastName(String lastName){
		this.lastName = lastName;
	}

	public String getLastName(){
		return lastName;
	}

	public void setGender(String gender){
		this.gender = gender;
	}

	public String getGender(){
		return gender;
	}

	public void setNameTitle(String nameTitle){
		this.nameTitle = nameTitle;
	}

	public String getNameTitle(){
		return nameTitle;
	}

	public void setCell(String cell){
		this.cell = cell;
	}

	public String getCell(){
		return cell;
	}

	public void setFirstName(String firstName){
		this.firstName = firstName;
	}

	public String getFirstName(){
		return firstName;
	}

	public void setPassword(String password){
		this.password = password;
	}

	public String getPassword(){
		return password;
	}

	public void setPhone(String phone){
		this.phone = phone;
	}

	public String getPhone(){
		return phone;
	}

	public void setDob(String dob){
		this.dob = dob;
	}

	public String getDob(){
		return dob;
	}

	public void setUserLocationResponse(UserLocationResponse userLocationResponse){
		this.userLocationResponse = userLocationResponse;
	}

	public UserLocationResponse getUserLocationResponse(){
		return userLocationResponse;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setEmail(String email){
		this.email = email;
	}

	public String getEmail(){
		return email;
	}

	public void setUsername(String username){
		this.username = username;
	}

	public String getUsername(){
		return username;
	}

	@Override
 	public String toString(){
		return 
			"UserDetailResponse{" + 
			"image = '" + image + '\'' + 
			",lastName = '" + lastName + '\'' + 
			",gender = '" + gender + '\'' + 
			",nameTitle = '" + nameTitle + '\'' + 
			",cell = '" + cell + '\'' + 
			",firstName = '" + firstName + '\'' + 
			",password = '" + password + '\'' + 
			",phone = '" + phone + '\'' + 
			",dob = '" + dob + '\'' + 
			",userLocationResponse = '" + userLocationResponse + '\'' +
			",id = '" + id + '\'' + 
			",email = '" + email + '\'' + 
			",username = '" + username + '\'' + 
			"}";
		}
}