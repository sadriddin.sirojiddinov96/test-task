package com.example.testtask.repo.pojo;

import com.example.testtask.repo.entity.User;
import com.google.gson.annotations.SerializedName;

public class UserItem {

	public static UserItem fromUser(User user){
		UserItem userItem = new UserItem();
		userItem.setId(user.getId());
		userItem.setFirstName(user.getFirstName());
		userItem.setLastName(user.getLastName());
		userItem.setImage(user.getImage());
		userItem.setNameTitle(user.getNameTitle());

		return userItem;
	}

	@SerializedName("firstName")
	private String firstName;

	@SerializedName("lastName")
	private String lastName;

	@SerializedName("image")
	private String image;

	@SerializedName("nameTitle")
	private String nameTitle;

	@SerializedName("id")
	private int id;

	public void setFirstName(String firstName){
		this.firstName = firstName;
	}

	public String getFirstName(){
		return firstName;
	}

	public void setLastName(String lastName){
		this.lastName = lastName;
	}

	public String getLastName(){
		return lastName;
	}

	public void setImage(String image){
		this.image = image;
	}

	public String getImage(){
		return image;
	}

	public void setNameTitle(String nameTitle){
		this.nameTitle = nameTitle;
	}

	public String getNameTitle(){
		return nameTitle;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	@Override
 	public String toString(){
		return 
			"UserItem{" +
			"firstName = '" + firstName + '\'' + 
			",lastName = '" + lastName + '\'' + 
			",image = '" + image + '\'' + 
			",nameTitle = '" + nameTitle + '\'' + 
			",id = '" + id + '\'' + 
			"}";
		}
}