package com.example.testtask.repo.pojo;

import com.example.testtask.repo.entity.UserLocation;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class UserLocationResponse implements Serializable {

	public static UserLocationResponse fromUserLocation(UserLocation userLocation){
		UserLocationResponse userLocationResponse = new UserLocationResponse();
		userLocationResponse.setStreet(userLocation.getStreet());
		userLocationResponse.setCity(userLocation.getCity());
		return userLocationResponse;
	}

	@SerializedName("city")
	private String city;

	@SerializedName("street")
	private String street;

	@SerializedName("timezone")
	private String timezone;

	@SerializedName("postcode")
	private int postcode;

	@SerializedName("state")
	private String state;

	public void setCity(String city){
		this.city = city;
	}

	public String getCity(){
		return city;
	}

	public void setStreet(String street){
		this.street = street;
	}

	public String getStreet(){
		return street;
	}

	public void setTimezone(String timezone){
		this.timezone = timezone;
	}

	public String getTimezone(){
		return timezone;
	}

	public void setPostcode(int postcode){
		this.postcode = postcode;
	}

	public int getPostcode(){
		return postcode;
	}

	public void setState(String state){
		this.state = state;
	}

	public String getState(){
		return state;
	}

	@Override
 	public String toString(){
		return 
			"UserLocationResponse{" +
			"city = '" + city + '\'' + 
			",street = '" + street + '\'' + 
			",timezone = '" + timezone + '\'' + 
			",postcode = '" + postcode + '\'' + 
			",state = '" + state + '\'' + 
			"}";
		}
}