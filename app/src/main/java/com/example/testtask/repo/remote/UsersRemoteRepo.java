package com.example.testtask.repo.remote;

import com.example.testtask.repo.entity.UserDetail;
import com.example.testtask.repo.pojo.DataResponse;
import com.example.testtask.repo.pojo.UserDetailResponse;

import java.util.List;

import io.reactivex.Observable;

public interface UsersRemoteRepo {

    Observable<DataResponse> getAllUsers();

    Observable<UserDetailResponse> getUserDetail(Long userId);

}
