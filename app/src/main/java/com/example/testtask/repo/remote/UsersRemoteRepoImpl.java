package com.example.testtask.repo.remote;

import android.content.Context;

import com.example.testtask.base.BaseRemote;
import com.example.testtask.base.RemoteConfiguration;
import com.example.testtask.repo.pojo.DataResponse;
import com.example.testtask.repo.pojo.UserDetailResponse;

import java.util.List;

import io.reactivex.Observable;

public class UsersRemoteRepoImpl extends BaseRemote implements UsersRemoteRepo {
    Context context;


    public UsersRemoteRepoImpl(Context context) {
        this.context = context;
    }

    @Override
    public Observable<DataResponse> getAllUsers() {
        return create(UsersServices.class, RemoteConfiguration.BASE_URL, context).getUserList();
    }

    @Override
    public Observable<UserDetailResponse> getUserDetail(Long userId) {
        return create(UsersServices.class, RemoteConfiguration.BASE_URL, context).getUserDetail(userId);
    }
}
