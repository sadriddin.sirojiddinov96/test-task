package com.example.testtask.repo.remote;

import com.example.testtask.repo.entity.UserDetail;
import com.example.testtask.repo.pojo.DataResponse;
import com.example.testtask.repo.pojo.UserDetailResponse;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface UsersServices {

    @GET("user")
    Observable<DataResponse> getUserList();

    @GET("user/{Id}")
    Observable<UserDetailResponse> getUserDetail(@Path("Id") Long userId);

}
